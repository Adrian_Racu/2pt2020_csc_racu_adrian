package Project2;



public class Polinom {
public int []coef;
public int degree;

public  Polinom(int a, int b) {
	if(b<0) {
		throw new IllegalArgumentException("nu poate fi negativ"+b);
}
	coef=new int[b+1];
	coef[b]=a;
	reduce();
	}
private void reduce() {
	degree=-1;
	for(int i=coef.length-1;i>=0;i--) {
		if(coef[i]!=0) {
		degree=i;
		return;
	}		
  }
}
public  int degree() {
	return degree;
}


public   Polinom plus( Polinom that) {	
	Polinom poly=new Polinom(0,Math.max(this.degree(), that.degree()));
	for(int i=0;i<=this.degree();i++) poly.coef[i]+=this.coef[i];
	for(int i=0;i<=that.degree();i++) poly.coef[i]+=that.coef[i];
	poly.reduce();
	return poly;}
	
	public Polinom min(Polinom that) {
	Polinom poly=new Polinom(0,Math.max(this.degree(), that.degree()));
	for(int i=0;i<=this.degree();i++) poly.coef[i]-=this.coef[i];
	for(int i=0;i<=that.degree();i++) poly.coef[i]-=that.coef[i];
	poly.reduce();
	return poly;}				
		
	public Polinom inmul(Polinom that) {
	Polinom poly=new Polinom(0,Math.max(this.degree(), that.degree()));
		for(int i=0;i<=this.degree();i++)
		for(int j=0;j<=that.degree();j++) 
		poly.coef[i+j]+=(this.coef[i]*that.coef[j]);
	    poly.reduce();	
	    return poly;}			
	public Polinom div(Polinom that) {
	Polinom poly=new Polinom(0,Math.max(this.degree,that.degree()));
		for(int i=0;i<=this.degree();i++)
		for(int j=0;j<=that.degree();j++) 
		poly.coef[i+j]+=(this.coef[i]/that.coef[j]);
		poly.reduce();	
		    return poly;}					
    public String toString() {
    	String s=coef[degree]+"x^"+degree;
    	for(int i=degree-1;i>=0;i--) {
    		  if      (degree == -1) return "0";
    	        else if (degree ==  0) return "" + coef[0];
    	        else if (degree ==  1) return coef[1] + "x + " + coef[0];
    	        s = coef[degree] + "x^" + degree;
    	        for ( i = degree - 1; i >= 0; i--) {
    	            if      (coef[i] == 0) continue;
    	            else if (coef[i]  > 0) s = s + " + " + (coef[i]);
    	            else if (coef[i]  < 0) s = s + " - " + (-coef[i]);
    	            if      (i == 1) s = s + "x";
    	            else if (i >  1) s= s + "x^" + i;
    	        }
    	   
    	
    
}
		return s;}}
